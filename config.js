var input_nodes2 = [
    {
        type: "transmitter",
        x: 200,
        y: 100,
        data: 1
    },
    {
        type: "transreceiver",
        x: 400,
        y: 100,
        slots: 1,
        data: 0
    },
    {
        type: "transreceiver",
        x: 300,
        y: 200,
        slots: 2,
        data: 0
    },
    {
        type: "transmitter",
        x: 400,
        y: 200,
        data: 1
    },
    {
        type: "receiver",
        x: 200,
        y: 300,
        slots: 1
    },
    {
        type: "receiver",
        x: 400,
        y: 300,
        slots: 1
    }
];

var input_nodes0 = [
    {
        type: "transmitter",
        x: 200,
        y: 100,
        data: 4
    },
    {
        type: "receiver",
        x: 100,
        y: 200,
        slots: 2
    },
    {
        type: "transreceiver",
        x: 200,
        y: 200,
        slots: 2,
        data: 0
    },
    {
        type: "transreceiver",
        x: 100,
        y: 300,
        slots: 1,
        data: 0
    },
    {
        type: "receiver",
        x: 200,
        y: 300,
        slots: 2
    },
    {
        type: "transreceiver",
        x: 300,
        y: 300,
        slots: 1,
        data: 0
    }
];

var input_nodes = [
    {
        type: "transreceiver",
        x: 200,
        y: 100,
        slots: 1,
        data: 1
    },
    {
        type: "transreceiver",
        x: 100,
        y: 200,
        slots: 2,
        data: 1
    },
    {
        type: "transreceiver",
        x: 300,
        y: 200,
        slots: 2,
        data: 0
    },
    {
        type: "receiver",
        x: 200,
        y: 300,
        slots: 2
    },
    {
        type: "transreceiver",
        x: 200,
        y: 400,
        slots: 2,
        data: 1
    }
];

var input_links = [
    {
        start: 0,
        end: 1
    },
    {
        start: 1,
        end: 2
    },
    {
        start: 3,
        end: 2
    },
    {
        start: 2,
        end: 4
    },
    {
        start: 2,
        end: 5
    }
];