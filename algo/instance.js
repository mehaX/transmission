var solutions = [];
var instance_count = 0;

var Instance = function(nodes_input, start_index, end_index, links_track)
{
    var nodes = JSON.parse(JSON.stringify(nodes_input));
    links_track = links_track || [];
    instance_count++;

    if (start_index || end_index)
    {
        nodes[start_index].add_link(nodes[end_index]);
        // output_nodes = nodes;

        if (has_completed_nodes(nodes))
        {
            solutions.push(links_track);
            return;
        }
    }

    var playable_nodes = get_playable_nodes(nodes);

    if (playable_nodes.length === 0)
    {
        var i = 0;
    }

    playable_nodes.forEach(function(playable_node)
    {
        var linkable_nodes = playable_node.node.linkable_nodes(nodes);

        if (!linkable_nodes)
        {
            var j = 0;
        }

        linkable_nodes.forEach(function(linkable_node)
        {
            new Instance(nodes,
                playable_node.node.index,
                linkable_node.node.index,
                links_track.concat({start_index: playable_node.node.index, end_index: linkable_node.node.index}));

            console.log(links_track, nodes);
            count++;
        });
    });
};

function get_playable_nodes(nodes)
{
    var res = [];

    nodes.forEach(function(node)
    {
        if (node.node.onhold > 0 && !(node instanceof Receiver))
            res.push(node);
    });

    return res;
}

function has_completed_nodes(nodes)
{
    var full_slots = true;

    nodes.forEach(function(node)
    {
        if (node.node.available() > 0)
            full_slots = false;
    });

    return full_slots;
}