<?php
set_time_limit(0);

require_once "entities/bootstrap.php";

$solutions = array();
$instance_count = 0;
$debug = false;

Instance();
if ($debug)
{
    echo "<h1>Solutions</h1>";
    var_dump($solutions, $instance_count);
}
else
{
    header("Content: application/json");
    echo json_encode($solutions);
}

function Instance($links_track = array())
{
    global $solutions;
    global $instance_count;
    global $debug;

    $iteration = $instance_count++;
    if ($instance_count > 100)
        return;
    if ($debug)
    {
        echo "<h1>Iteration: $instance_count</h1>";
        echo '<div style="display: flex">';
    }

    $nodes = generate_nodes($links_track);
    if (has_completed_nodes($nodes))
    {
        array_push($solutions, $links_track);
    }

    if ($debug)
    {
        echo '<div style="width:400px;float:left;">';
        echo '<h3>nodes</h3>';
        var_dump($nodes);
        echo '</div>';
    }

    $playable_nodes = get_playable_nodes($nodes);
    if ($debug)
    {
        echo '<div style="width:400px;float:left;">';
        echo '<h3>playable_nodes</h3>';
        var_dump($playable_nodes);
        echo '</div>';

        echo '<div style="width:400px;float:left;">';
        echo '<h3>links_track</h3>';
        var_dump($links_track);
        echo '</div>';

        echo '</div>';
        echo "<hr>";
    }

    foreach ($playable_nodes as $playable_node)
    {
        $linkable_nodes = $playable_node->linkable_nodes($nodes);
        foreach ($linkable_nodes as $linkable_node)
        {
            if ($debug)
            {
                echo "<h2>Jumping to iteration: ".($instance_count + 1)."</h2>";
                echo '<div style="display: flex">';
                echo '<div style="width:400px;float:left;">';
                echo "<h3>From</h3>";
                var_dump($playable_node);
                echo '</div>';
                echo '<div style="width:400px;float:left;">';
                echo '<h3>To</h3>';
                var_dump($linkable_node);
                echo '</div>';
                echo '</div>';
            }

            Instance(
                array_merge($links_track, [
                    [
                        'start_index' => $playable_node->index,
                        'end_index' => $linkable_node->index
                    ]
                ]));
            $nodes = generate_nodes($links_track);

            if ($debug)
            {
                echo "<h2>Back to iteration: $iteration</h2>";
                echo '<div style="display: flex">';
                echo '<div style="width:400px;float:left;">';
                echo "<h3>nodes</h3>";
                var_dump($nodes);
                echo '</div>';
                echo '<div style="width:400px;float:left;">';
                echo '<h3>links_track</h3>';
                var_dump($links_track);
                echo '</div>';
                echo '</div>';
            }
        }
    }
}

function get_playable_nodes($nodes)
{
    $res = [];

    foreach ($nodes as $node)
        if ($node->onhold > 0
            && !($node instanceof Receiver))
            array_push($res, $node);

    return $res;
}

function has_completed_nodes($nodes)
{
    $full_slots = true;

    foreach ($nodes as $node)
        if ($node->available() > 0)
            $full_slots = false;

    return $full_slots;
}

function generate_nodes($links_track)
{
    $nodes = read_nodes();

    foreach ($links_track as $link_track)
    {
        $nodes[$link_track['start_index']]->add_link($nodes[$link_track['end_index']]);
    }

    return $nodes;
}

function read_nodes()
{
    $index = 0;
    $input_nodes = json_decode(file_get_contents('nodes.json'), true);
    $nodes = array();
    foreach ($input_nodes as $inode)
    {
        switch($inode['type'])
        {
            case "transmitter":
                $node = new Transmitter($inode['x'], $inode['y'], $inode['data']);
                break;

            case "transreceiver":
                $node = new Transreceiver($inode['x'], $inode['y'], $inode['slots'], $inode['data'], $inode['data']);
                break;

            case "receiver":
                $node = new Receiver($inode['x'], $inode['y'], $inode['slots']);
                break;

            default:
                return;
        }
        $node->index = $index++;
        array_push($nodes, $node);
    }
    return $nodes;
}