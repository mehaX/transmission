<?php
/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 11/15/2017
 * Time: 3:57 PM
 */

require_once "Node.php";

class Transmitter extends Node
{
    public function __construct($x, $y, $data)
    {
        parent::__construct($x, $y, $data, $data, $data);
    }

    public function transfer()
    {
        $changed = false;

        if ($this->onhold > 0)
        {
            foreach ($this->links as $link) if ($link->available() > 0)
            {
                $add_data = $link->add_data($this->onhold);
                $this->onhold -= $add_data;
                $changed = true;
            }
        }

        return $changed;
    }
}