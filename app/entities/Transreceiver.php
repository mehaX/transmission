<?php
/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 11/15/2017
 * Time: 4:01 PM
 */

require_once "Node.php";

class Transreceiver extends Node
{
    public function __construct($x, $y, $slots = 0, $data = 0, $onhold = 0)
    {
        parent::__construct($x, $y, $slots, $data, $onhold);
    }

    public function transfer()
    {
        $changed = false;

        if ($this->onhold > 0)
        {
            foreach ($this->links as $link) if ($link->available() > 0)
            {
                $this->onhold -= $link->add_data($this->onhold);
                $changed = true;
            }
        }

        return $changed;
    }
}
