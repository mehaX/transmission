<?php
/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 11/15/2017
 * Time: 3:59 PM
 */

require_once "Node.php";

class Receiver extends Node
{
    public function __construct($x, $y, $slots)
    {
        parent::__construct($x, $y, $slots);
    }

    public function transfer()
    {
        return false;
    }
}