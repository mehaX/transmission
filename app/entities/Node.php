<?php
/**
 * Created by PhpStorm.
 * User: mehax
 * Date: 11/15/2017
 * Time: 3:40 PM
 */

abstract class Node
{
    public $index;
    public $x;
    public $y;

    public $slots;
    public $data;
    public $onhold;
    public $links = [];

    public function __construct($x, $y, $slots = 0, $data = 0, $onhold = 0)
    {
        $this->x = $x;
        $this->y = $y;
        $this->slots = $slots;
        $this->data = $data;
        $this->onhold = $onhold;
    }

    public function available()
    {
        return $this->slots - $this->data;
    }

    public function add_data($new_data)
    {
        $add_data = min($new_data, $this->available());

        $this->data += $add_data;
        $this->onhold += $add_data;

        return $add_data;
    }

    public function add_link($node)
    {
        array_push($this->links, $node);

        return $this->transfer();
    }

    public function is_connected($node)
    {
        return in_array($node, $this->links);
    }

    public function distance($node)
    {
        return sqrt(pow($this->x - $node->x, 2) + pow($this->y - $node->y, 2));
    }

    public function equals($node)
    {
        return $this->x == $node->x && $this->y == $node->y;
    }

    public function linkable_nodes(array $nodes)
    {
        $A = $this;
        $res = [];

        foreach ($nodes as $B)
        {
            if ($A->index == 3 && $B->index == 5)
            {
                $i = 0;
            }

            if (!$A->equals($B))
            {
                $AB = $A->distance($B);

                $collision = false;
                foreach ($nodes as $C)
                {
                    $AC = $A->distance($C);
                    $BC = $B->distance($C);

                    if (!$C->equals($A) && !$C->equals($B) && $AB > $AC)
                    {
                        $cosA = (pow($AB, 2) - pow($AC, 2) - pow($BC, 2)) / (2.0 * $AC * $BC);
                        $Alpha = acos($cosA);

                        if ($Alpha < 0.5)
                            $collision = true;
                    }
                }

                $t1 = !$B->is_connected($A);
                $t2 = !$A->is_connected($B);
                $t3 = $B->available() > 0;

                if ($t1 && $t2 && $t3 && !$collision)
                    array_push($res, $B);
            }
        }

        return $res;
    }

    public abstract function transfer();

    public function reverse(&$node)
    {
        foreach ($this->links as $key => $link)
            if ($link->index == $node->index)
            {
                unset($this->links[$key]);
                break;
            }

        if ($node->onhold > 0)
        {
            $transfer = min($this->available(), $node->onhold);

            $node->onhold -= $transfer;
            $node->data -= $transfer;

            $this->onhold += $transfer;

            return $transfer;
        }
        return 0;
    }
}