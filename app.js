
var output_nodes = [];
var index = 0;

function setup()
{
    createCanvas(800, 600);

    var index = 0;
    input_nodes.forEach(function(inode)
    {
        var node = null;
        switch(inode.type)
        {
            case "transmitter":
                node = new Transmitter(inode.x, inode.y, inode.data);
                break;

            case "transreceiver":
                node = new Transreceiver(inode.x, inode.y, inode.slots, inode.data);
                break;

            case "receiver":
                node = new Receiver(inode.x, inode.y, inode.slots);
                break;

            default:
                return;
        }
        node.node.index = index++;
        output_nodes.push(node);
    });
}

function draw()
{
    background(128);

    output_nodes.forEach(function(node)
    {
        node.draw();
    });
}

function keyPressed()
{
    if (keyCode == 13)
    {
        new Instance(output_nodes);
        console.log(solutions);
    }
}