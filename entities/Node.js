var count = 0;

var Node = function(x, y, slots, data, onhold)
{
    this.x      = x;
    this.y      = y;

    this.slots  = slots ? slots : 0;
    this.data   = data ? data : 0;
    this.onhold = onhold ? onhold : 0;
    this.links  = [];

    this.available = function()
    {
        return this.slots - this.data;
    };

    this.add_data = function(new_data)
    {
        var add_data = Math.min(new_data, this.available());

        this.data += add_data;
        this.onhold += add_data;

        return add_data;
    };

    this.draw_slots = function()
    {
        for (var i = 0; i < this.slots; i++)
        {
            if (i < this.data)
                fill(255);
            else
                noFill();

            stroke(255);
            if (i < 4)
            {
                var x = 10 + parseInt(i % 2) * 20 - 25;
                var y = 10 + parseInt(i / 2) * 20 - 25;

                rect(this.x + x, this.y + y, 10, 10);
            }
            else
                rect(this.x - 5, this.y - 5, 10, 10);
        }
    };

    this.draw_onhold = function()
    {
        if (this.onhold > 0)
        {
            textSize(15);
            stroke(255);
            text("" + this.onhold, this.x + 30, this.y + 15);
        }
    };

    this.draw_links = function()
    {
        stroke(255);
        var self = this;
        this.links.forEach(function(link)
        {
            line(self.x, self.y, link.node.x, link.node.y);
        });
    };

    this.add_link = function(node)
    {
        this.links.push(node);

        if (this.transfer)
            return this.transfer();
        return false;
    };

    this.is_connected = function(node)
    {
        var res = false;
        this.links.forEach(function(link)
        {
            if (link === node)
                res = true;
        });
        return res;
    };

    this.distance = function(node)
    {
        return Math.sqrt(Math.pow(this.x - node.x, 2) + Math.pow(this.y - node.y, 2));
    };

    this.linkable_nodes = function(nodes)
    {
        var Anode = this;
        var res = [];
        nodes.forEach(function(B)
        {
            var Bnode = B.node;
            if (Anode.index !== Bnode.index)
            {
                var AB = Anode.distance(Bnode);
                var collision = false;

                nodes.forEach(function(C)
                {
                    var Cnode = C.node;

                    var AC = Anode.distance(Cnode);
                    var BC = Bnode.distance(Cnode);

                    if (Cnode.index !== Anode.index && Cnode.index !== Bnode.index && AB > AC)
                    {
                        var cosA = (Math.pow(AB, 2) - Math.pow(AC ,2) - Math.pow(BC, 2)) / (2.0 * AB * AC);
                        var Alpha = Math.acos(cosA);

                        if (Alpha < 0.5)
                            collision = true;
                    }
                });

                if (!Bnode.is_connected(Anode) && !Anode.is_connected(Bnode) && Bnode.available() > 0 && !collision)
                    res.push(B);
            }
        });
        return res;
    };
};
