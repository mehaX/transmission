
var Receiver = function(x, y, slots)
{
    this.node = new Node(x, y, slots);

    this.draw = function()
    {
        this.node.draw_links();

        stroke(255);
        fill(128);

        rect(this.node.x - 25, this.node.y - 25, 50, 50);

        this.node.draw_slots();
        this.node.draw_onhold();
        this.node.draw_links();
    };

    this.transfer = function()
    {
        return this.node.transfer();
    };
    this.is_connected = function(node)
    {
        return this.node.is_connected(node);
    };
    this.distance = function(node)
    {
        return this.node.distance(node);
    };
    this.linkable_nodes = function(nodes)
    {
        return this.node.linkable_nodes(nodes);
    };
    this.add_link = function(node)
    {
        return this.node.add_link(node);
    };
    this.available = function()
    {
        return this.node.available();
    };
};
