
var Transreceiver = function(x, y, slots, data)
{
    this.node = new Node(x, y, slots, data, data);

    this.draw = function()
    {
        this.node.draw_links();

        stroke(255);
        fill(128);

        ellipse(this.node.x, this.node.y, 50, 50);

        this.node.draw_slots();
        this.node.draw_onhold();
        this.node.draw_links();
    };

    this.node.transfer = function()
    {
        var changed = false;
        if (this.onhold > 0)
        {
            var self = this;
            this.links.forEach(function(link)
            {
                if (link.node.available() > 0)
                {
                    self.onhold -= link.node.add_data(self.onhold);
                    changed = true;
                }
            });
        }
        return changed;
    };

    this.transfer = function()
    {
        return this.node.transfer();
    };
    this.is_connected = function(node)
    {
        return this.node.is_connected(node);
    };
    this.distance = function(node)
    {
        return this.node.distance(node);
    };
    this.linkable_nodes = function(nodes)
    {
        return this.node.linkable_nodes(nodes);
    };
    this.add_link = function(node)
    {
        return this.node.add_link(node);
    };
    this.available = function()
    {
        return this.node.available();
    };
};
